<?php

namespace Interfaces;

interface File {
    public function getUrl();
    public function delete();
}