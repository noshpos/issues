<?php

namespace Models;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Type
 * @package Models
 * @ORM\MappedSuperclass()
 */
abstract class Type extends Model {

}