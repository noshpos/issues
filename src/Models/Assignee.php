<?php

namespace Models;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Assignee
 * @package Models
 * @ORM\MappedSuperclass()
 */
abstract class Assignee extends Model {

}