<?php

namespace Models;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Status
 * @package Models
 * @ORM\MappedSuperclass()
 */
class Status extends Model {
    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $name;

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }
}