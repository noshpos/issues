<?php

namespace Models;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class StatusGroup
 * @package Models
 * @ORM\MappedSuperclass()
 */
abstract class StatusGroup extends Model {
    /**
     * @var ArrayCollection
     */
    protected $name;

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }

    /**
     * @var ArrayCollection
     */
    protected $statuses;

    public function getStatuses() {
        return $this->statuses;
    }

    public function addStatus(Status $status) {
        $this->statuses->add($status);
    }

    public function removeStatus(Status $status) {
        $this->statuses->removeElement($status);
    }
}