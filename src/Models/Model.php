<?php

namespace Models;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Model
 * @package Models
 * @ORM\MappedSuperclass()
 */
abstract class Model {
    /**
     * @var integer
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue()
     * @ORM\Id()
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="datetime", name="created_on")
     */
    protected $createdOn;

    /**
     * @var string
     * @ORM\Column(type="datetime", name="modified_on")
     */
    protected $modifiedOn;
}