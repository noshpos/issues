<?php

namespace Models;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Issue
 * @package Model
 * @ORM\Table(name="issues")
 * @ORM\MappedSuperclass()
 */
abstract class Issue extends Model {
    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $title;

    public function getTitle() {
        return $this->title;
    }

    public function setTitle($title) {
        $this->title = $title;
    }

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    protected $description;

    public function getDescription() {
        return $this->description;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    /**
     * @var ArrayCollection
     */
    protected $replies;


    public function getReplies() {
        return $this->replies;
    }

    public function addReply(Reply $reply) {
        $this->replies->add($reply);
    }

    public function removeReply(Reply $reply) {
        $this->replies->removeElement($reply);
    }
}