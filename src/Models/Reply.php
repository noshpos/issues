<?php

namespace Models;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Reply
 * @package Models
 * @ORM\MappedSuperclass()
 */
class Reply extends Model {
    /**
     * @var
     * @ORM\Column(type="text")
     */
    protected $message;
}