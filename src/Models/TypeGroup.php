<?php

namespace Models;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class TypeGroup
 * @package Models
 * @ORM\MappedSuperclass()
 */
abstract class TypeGroup extends Model {
    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $name;

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }

    /**
     * @var ArrayCollection
     */
    protected $types;

    public function getTypes() {
        return $this->types;
    }

    public function addType(Type $type) {
        $this->types->add($type);
    }

    public function removeType(Type $type) {
        $this->types->removeElement($type);
    }
}